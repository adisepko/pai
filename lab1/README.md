To use add below entries to C:\Windows\System32\drivers\etc\hosts for windows or /etc/resolv.conf on linux:<br>
127.0.0.1 student.pl <br>
127.0.0.1 www.student.pl <br>
127.0.0.1 wykladowca.pl <br>
127.0.0.1 www.wykladowca.pl <br>
127.0.0.1 wykladowca80.pl <br>
127.0.0.1 www.wykladowca80.pl <br>
127.0.0.1 student80.pl <br>
127.0.0.1 www.student80.pl <br>
<br>
sites with 80 in name run on port 8080, so you need to specify that in browser e.g. student80.pl:8080
